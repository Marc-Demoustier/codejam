n = int(input())
results = []
for _ in range(n):
    nbs = input().split(" ")
    persons = int(nbs[0])
    size = int(nbs[1]) + 1
    xs = [0] * size
    ys = [0] * size
    for _ in range(persons):
        coords = input().split(" ")
        x = int(coords[0])
        y = int(coords[1])
        if coords[2] == "N":
            for j in range(y+1, size):
                ys[j] +=1
        elif coords[2] == "S":
            for j in range(y-1, -1, -1):
                ys[j] += 1
        elif coords[2] == "E":
            for j in range(x+1, size):
                xs[j]+= 1
        else:
            for j in range(x-1, -1,-1):
                xs[j] += 1
    mx = 0
    my = 0
    for i in range(size):
        if xs[i] > xs[mx]:
            mx = i
        if ys[i] > ys[my]:
            my = i
    results.append((mx, my))

for i in range(n):
    print("Case #" + str(i+1) + ": " + str(results[i][0]) + " " + str(results[i][1]))




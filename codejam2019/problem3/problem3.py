def foregone_solution(s):
    n1 = ""
    n2 = ""
    for char in s:
        if char == '4':
            n1 += '3'
            n2 += '1'
        else:
            n1 += char
            if(len(n2) != 0):
                n2 += '0'
    return n1, n2

if __name__ == '__main__':
    results = []
    number_of_case = int(input())
    for i in range(number_of_case):
        n1, n2 = foregone_solution(input())
        result = "Case #" + str(i+1) + ": " + n1 + " " + n2
        results.append(result)
    for element in results:
        print(element)

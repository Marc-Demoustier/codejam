import math

def factors(n):
    F = []
    if n==1:
        return F
    while n>=2:
        x,r = divmod(n,2)
        if r!=0:
            break
        F.append(2)
        n = x
    i=3
    rn = math.sqrt(n)+1
    while i<=n:
        if i>rn:
            F.append(n)
            break
        x,r = divmod(n,i)
        if r==0:
            F.append(i)
            n=x
            rn = math.sqrt(n)+1
        else:
            i += 2
    return F


def create_alphabet(T, L):
    alphabet = []
    letters = []
    i = 0
    while T[i] == T[i+1]:
        i += 1
    factor = factors(T[i])
    if factor[0] == factor[1]:
        pres_factor = factor[0]
        if not pres_factor in alphabet:
            alphabet.append(pres_factor)
        letters.insert(0, pres_factor)
        letters.insert(0, pres_factor)
        post_factor = T[i + 1] // factor[0]
        letters.append(post_factor)
        if not post_factor in alphabet:
            alphabet.append(post_factor)
    else:
        for prime in factor:
            if not prime in alphabet:
                alphabet.append(prime)
            if T[i + 1] % prime == 0:
                letters.append(prime)
                post_factor = T[i + 1] // prime
                letters.append(post_factor)
                if not post_factor in alphabet:
                    alphabet.append(post_factor)
            else:
                pres_factor = prime
                if not pres_factor in alphabet:
                    alphabet.append(pres_factor)
                letters.insert(0, prime)
    for j in range(i-1, -1, -1):
        pres_factor = T[j] // pres_factor
        letters.insert(0, pres_factor)
        if not pres_factor in alphabet:
            alphabet.append(pres_factor)
    for j in range(i+2, L):
        post_factor = T[j] // post_factor
        letters.append(post_factor)
        if not post_factor in alphabet:
            alphabet.append(post_factor)
    alphabet.sort()
    return alphabet, letters

def translate(alphabet, letters):
    result = ""
    for letter in letters:
        result += chr(alphabet.index(letter) + 65)
    return result


if __name__ == '__main__':
    number_of_case = int(input())
    for i in range(number_of_case):
        L = int(input().split()[1])
        T = [int(e) for e in input().split()]
        alphabet, letters = create_alphabet(T, L)
        result = translate(alphabet, letters)
        print("Case #" + str(i + 1) + ": " + result)

def resolve_maze(path):
    result = ""
    for char in path:
        if char == 'E':
            result += "S"
        else:
            result += 'E'
    return result

if __name__ == '__main__':
    results = []
    number_of_case = int(input())
    for i in range(number_of_case):
        input()
        path = input()
        result = "Case #" + str(i + 1) + ": " + resolve_maze(path)
        results.append(result)
    for element in results:
        print(element)

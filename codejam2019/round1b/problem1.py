def manhattan_crepe(size, elements):
    x = [0] * size
    y = [0] * size
    for element in elements:
        if element[2] == 'N':
            for i in range(int(element[1]) + 1, size):
                y[i] += 1
        if element[2] == 'S':
            for i in range(int(element[1]) - 1, -1, -1):
                y[i] += 1
        if element[2] == 'E':
            for i in range(int(element[0]) + 1, size):
                x[i] += 1
        if element[2] == 'W':
            for i in range(int(element[0]) - 1, -1, -1):
                x[i] += 1
    return (x, y)

def max_x_y(size, x, y):
    max_x = 0
    max_y = 0
    for i in range(size):
        if x[max_x] < x[i]:
            max_x = i
        if y[max_y] < y[i]:
            max_y = i
    return max_x, max_y

if __name__ == '__main__':
    number_of_case = int(input())
    for i in range(number_of_case):
        line = input().split(" ")
        P = int(line[0])
        size = int(line[1]) + 1
        elements = []
        for j in range(P):
            elements.append(input().split(" "))
        x, y = manhattan_crepe(size, elements)
        max_x, max_y = max_x_y(size, x, y)
        print("Case #" + str(i+1) + ": " + str(max_x) + " " + str(max_y))

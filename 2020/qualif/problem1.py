def compute_trace(mat, size):
    trace = 0
    for i in range(size):
        trace += mat[i][i]
    return trace

def row_duplicate(mat, size):
    duplicate = 0
    for i in range(size):
        if size > len(set(mat[i])):
            duplicate += 1
    return duplicate

def column_duplicate(mat, size):
    duplicate = 0
    for i in range(size):
        seen = set()
        for j in range(size):
            seen.add(mat[j][i])
        if len(seen) < size:
            duplicate += 1
    return duplicate

def compute_result(mat, size):
    trace = compute_trace(mat, size)
    row_d = row_duplicate(mat, size)
    column_d = column_duplicate(mat, size)
    return str(trace) + " " + str(row_d) + " " + str(column_d)

if __name__ == '__main__':
    number_of_case = int(input())
    for i in range(number_of_case):
        size = int(input())
        mat = []
        for j in range(size):
            mat.append([int(e) for e in input().split()])
        result = compute_result(mat, size)
        print("Case #" + str(i + 1) + ": " + result)

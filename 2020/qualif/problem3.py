def is_disponible(activity, schedule):
    for used_time in schedule:
        if not (activity[1] <= used_time[0] or activity[0] >= used_time[1]):
            return False
    return True

def valid_schedule(activities):
    c = []
    j = []
    result = ""
    for activity in activities:
        if is_disponible(activity, c):
            result += "C"
            c.append(activity)
        elif is_disponible(activity, j):
            result += "J"
            j.append(activity)
        else:
            result = "IMPOSSIBLE"
            break
    return result

if __name__ == '__main__':
    number_of_case = int(input())
    for i in range(number_of_case):
        activities_number = int(input())
        activities = []
        for j in range(activities_number):
            activities.append([int(e) for e in input().split()])
        result = valid_schedule(activities)
        print("Case #" + str(i + 1) + ": " + result)

def matching_parenthesis(S):
    open_parenthesis = 0
    result = ""
    for element in S:
        while open_parenthesis < int(element):
            result += "("
            open_parenthesis += 1
        while open_parenthesis > int(element):
            result += ")"
            open_parenthesis -= 1
        result += element
    while open_parenthesis > 0:
        result += ")"
        open_parenthesis -= 1
    return result

if __name__ == '__main__':
    number_of_case = int(input())
    for i in range(number_of_case):
        S = input()
        result = matching_parenthesis(S)
        print("Case #" + str(i + 1) + ": " + result)
